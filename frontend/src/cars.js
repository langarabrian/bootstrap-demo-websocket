import React, { Component } from 'react';

class Cars extends Component {
  constructor(props) {
    super(props);
    this.state = {
      make: '',
      model: '',
      year: '2020',
      plate: '',
      formclass: '',
      plateMessage: 'A valid license plate is required.',
      plateClass: 'form-control'
    };

    this.handleMakeChange = this.handleMakeChange.bind(this);
    this.handleModelChange = this.handleModelChange.bind(this);
    this.handleYearChange = this.handleYearChange.bind(this);
    this.handlePlateChange = this.handlePlateChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  handleMakeChange(event) {
    this.setState({make: event.target.value});
  }

  handleModelChange(event) {
    this.setState({model: event.target.value});
  }

  handleYearChange(event) {
    this.setState({year: event.target.value});
  }

  handlePlateChange(event) {
    this.setState({plate: event.target.value.toUpperCase()});
  }

  deleteCar(id, ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    const { carService } = this.props;

    /////////////////////////////////////////////////
    // WRITE THE CODE HERE TO REMOVE THE SELECTED CAR
    // START v
    /////////////////////////////////////////////////
    carService.remove( id );
    /////////////////////////////////////////////////
    // END ^
    /////////////////////////////////////////////////
  }
  
  handleSubmit(ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    const { carService } = this.props;
    
    ///////////////////////////////////////
    // WRITE THE CODE HERE TO ADD A NEW CAR
    // START v
    ///////////////////////////////////////

    if (ev.target.checkValidity()) {
      carService.create({
        make: this.state.make,
        model: this.state.model,
        year: this.state.year,
        plate: this.state.plate
      })
      .then(() => {
        console.log("Created new car.")
        this.setState({
          make: '',
          model: '',
          year: '2020',
          plate: '',
          formclass: '',
          plateMessage: 'A valid license plate is required.',
          plateClass: 'form-control'
        });      
      })
      .catch(error => {
        this.setState({
          formclass: '',
          plateMessage: error.message,
          plateClass: 'form-control is-invalid'
        });
      });
    } else {
      this.setState({
        formclass: 'was-validated',
        plateMessage: 'A valid license plate is required.',
        plateClass: 'form-control'
      });
    }

    ///////////////////////////////////////
    // END ^
    ///////////////////////////////////////
    
    ev.preventDefault();
  }

  render() {
    const { cars } = this.props;

    return(
    <div>
      <div className="py-5 text-center">
        <h2>Cars</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.handleSubmit} className={this.state.formclass} noValidate id="carform">
            <div className="row">
              <div className="col-lg-4 col-md-6 mb-3">
                <label htmlFor="make">Make</label>
                <input type="text" className="form-control" id="make" maxLength="5"
                  value={this.state.make} required onChange={this.handleMakeChange} />
                <div className="invalid-feedback">
                    A car make is required.
                </div>
              </div>

              <div className="col-lg-4 col-md-6 mb-3">
                <label htmlFor="model">Model</label>
                <input type="text" className="form-control" id="model" value={this.state.model} required onChange={this.handleModelChange} />
                <div className="invalid-feedback">
                    A car model is required.
                </div>
              </div>

              <div className="col-lg-2 col-sm-6 mb-3">
                <label htmlFor="year">Year</label>
                <input type="number" className="form-control" id="year" min="1885" max="2020"
                  value={this.state.year} required onChange={this.handleYearChange} />
                <div className="invalid-feedback">
                    A model year is required.
                </div>
              </div>

              <div className="col-lg-2 col-sm-6 mb-3">
                <label htmlFor="plate">Plate</label>
                <input type="text" className={this.state.plateClass} id="plate" pattern="[A-Z0-9]{1,6}"
                  value={this.state.plate} required onChange={this.handlePlateChange} />
                <div className="invalid-feedback">
                    {this.state.plateMessage}
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add car</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Make</th>
            <th scope="col">Model</th>
            <th scope="col">Year</th>
            <th scope="col">Plate</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>

          {cars && cars.map(car => <tr key={car.id}>
            <th scope="row">{car.id}</th>
            <td>{car.make}</td>
            <td>{car.model}</td>
            <td>{car.year}</td>
            <td>{car.plate}</td>
            <td><button onClick={this.deleteCar.bind(this, car.id)} type="button" className="btn btn-danger">Delete</button></td>
          </tr>)}

        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Cars;
