import io from 'socket.io-client';
import feathers from '@feathersjs/client';

const socket = io('http://midterm-backend.494901.xyz');

const client = feathers();

// YOU HAVE TO UPDATE THIS WITH YOUR OWN DOMAIN NAME

client.configure(feathers.socketio(socket));

export default client;
